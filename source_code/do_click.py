from DrissionPage import ChromiumPage


def do_click(page: ChromiumPage, option_length: int, answer_list: list, result: int):
    # 单选题&多选题
    if option_length == 2 and result != 3 and len(answer_list) <= 2:
        page.wait.load_start()
        A = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]/text()[3]')
        B = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]/text()[3]')
        print("选项A的值为:" + A)
        print("选项B的值为:" + B)
        if A in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')
        if B in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中B答案')
        if ((A not in answer_list) and (B not in answer_list)) or len(answer_list) > 2:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')
        click_button = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[2]/button')
        click_button.click()
        return page
    if option_length == 3 and result != 3 and len(answer_list) <= 3:
        page.wait.load_start()
        A = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]/text()[3]')
        B = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]/text()[3]')
        C = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[3]/text()[3]')
        print("选项A的值为:" + A)
        print("选项B的值为:" + B)
        print("选项C的值为:" + C)
        if A in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')
        if B in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中B答案')
        if C in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[3]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中C答案')

        if len(answer_list) > 3:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')

        click_button = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[2]/button')
        click_button.click()
        return page

    if option_length == 4 and result != 3 and len(answer_list) <= 4:
        page.wait.load_start()
        A = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]/text()[3]')
        B = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]/text()[3]')
        C = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[3]/text()[3]')
        D = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[4]/text()[3]')
        print("选项A的值为:" + A)
        print("选项B的值为:" + B)
        print("选项C的值为:" + C)
        print("选项D的值为:" + D)
        if A in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')
        if B in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[2]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中B答案')
        if C in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[3]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中C答案')
        if D in answer_list:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[4]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中D答案')
        if len(answer_list) > 4:
            ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[4]/div[1]')
            ele.wait.display()
            ele.click(by_js=True)
            print('已选中A答案')
        click_button = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[2]/button')
        click_button.click()
        return page

    # 填空题
    if result == 3:
        ele = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[1]/div[2]/div/input')
        ele.wait.display()
        ele.input(answer_list[0])
        print('已填入答案')
        click_button = page.ele('xpath://*[@id="app"]/div/div[2]/div/div[4]/div[2]/button')
        click_button.click()
        return page 
