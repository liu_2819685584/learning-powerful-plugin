from DrissionPage import ChromiumPage

# 判断题目类型，返回数值和page
def judge_question_type(page: ChromiumPage) -> tuple[int,ChromiumPage,int]:
    type_text = page.ele('xpath://div[@class="q-header"]/text()')
    answer_list = page.eles('xpath://*[@id="app"]//div[@class="q-answers"]//div')
    # print(answer_list)
    option_length = len(answer_list)    # 判断总共有多少个选项
    print(f"总共有{option_length}个选项")
    if type_text == '单选题':
        print("题目类型为单选题")
        return 1,page,option_length
    if type_text == '多选题':
        print("题目类型为多选题")
        return 2, page,option_length
    if type_text == '填空题':
        print("题目类型为填空题")
        return 3, page,option_length 
