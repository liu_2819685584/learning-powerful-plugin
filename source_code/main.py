import time
import urllib

from DrissionPage import ChromiumPage
from judge_question_type import judge_question_type
from do_click import do_click

page = ChromiumPage()
page.clear_cache(session_storage=True,cache=True,cookies=True,local_storage=True)
page.get('https://pc.xuexi.cn/points/login.html?ref=https%3A%2F%2Fwww.xuexi.cn%2F')
page.scroll.to_bottom()
iframe = page.get_frame('#ddlogin-iframe')
qr_code = iframe.ele('tag:img').attr('src')
page.wait.load_start()
urllib.request.urlretrieve(qr_code, filename='login_QRcode.jpg')
print('登录二维码下载完成')
# "我要选读文章"模块
for i in range(1, 7):
    page.ele(
        f'xpath://*[@id="231c"]/div/div/div/div/div/section/div/div/div/div/div[2]/section/div/div/div/div[1]/div[{i}]/div/div/div/span').click()
    time.sleep(5)
    page.to_tab(page.latest_tab)
    page.scroll.to_half()
    time.sleep(30)
    page.scroll.to_bottom()
    time.sleep(30)
    print(f'已阅读第{i}篇文章')
    page.to_tab(page.latest_tab)
    page.close_tabs()
print("已获取读文章模块积分")
print("===============\n" * 5)

# "视听"模块
page.new_tab(url='https://www.xuexi.cn/4426aa87b0b64ac671c96379a3a8bd26/db086044562a57b441c24f2af1c8e101.html#1novbsbi47k-5')
page.wait.ele_display('text=学习强国>>学习电视台片库')
print("视频页面加载完成")
page.to_tab(page.tabs[0])
page.close_other_tabs()
print("当前页面URL为：" + page.url)
# time.sleep(5)
# page.to_tab(page.tabs[1])
# print("page.tabs[1]的URL为：" + page.url)
for row in range(1,2):
    for col in range(1,3):
        page.wait.ele_display(f'xpath://*[@id="1novbsbi47k-5"]/div/div/div/div/div/div/section/div[3]/section/div/div/div[1]/div[{row}]/div[{col}]/section/div/div/div/div/div[3]/div/div/div',timeout=40)
        page.ele(f'xpath://*[@id="1novbsbi47k-5"]/div/div/div/div/div/div/section/div[3]/section/div/div/div[1]/div[{row}]/div[{col}]/section/div/div/div/div/div[3]/div/div/div').click()
        page.wait.load_start()
        print('正在观看视频')
        time.sleep(5)
        page.scroll.to_half()
        time.sleep(30)
        page.scroll.to_bottom()
        time.sleep(30)
        print(f"已收看第{row}行第{col}个视频")
        # 关闭第1、3个标签页
        tabs = page.tabs
        print(tabs)
        page.close_tabs(tabs_or_ids=tabs[0])
        print("已关闭打开的视频tab")
print("已获取看视频模块积分")
print("===============\n" * 5)


# 答题模块
page.new_tab(url='https://pc.xuexi.cn/points/exam-practice.html')
page.wait.load_start()
page.to_tab(page.tabs[0])
page.close_other_tabs()
# print("当前页面URL为：" + page.url)
for i in range(1,6):
    print(f'第{i}题')
    # 点击页面的"查看提示"按钮
    page.ele('text=查看提示').click()
    page.wait.load_start()
    # 返回题目类型（单选题=1、多选题=2、填空题=3）、page、选项个数
    result, page, option_length = judge_question_type(page)
    answer_list = page.eles('xpath://*[@id="body-body"]/div[4]/div/div/div/div[2]/div/div/div/font/text()')
    print("答案为:", end='')
    print(answer_list)
    page = do_click(page=page,option_length=option_length,answer_list=answer_list,result=result)
    print("==============\n" * 2)
print("已获取答题模块积分")

